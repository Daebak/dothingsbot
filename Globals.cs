﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using DoThingsBot.Views;

namespace DoThingsBot
{
	public static class Globals
	{
		public static void Init(string pluginName, PluginHost host, CoreManager core)
		{
			PluginName = pluginName;

			Host = host;

			Core = core;
        }

		public static string PluginName { get; private set; }

		public static PluginHost Host { get; private set; }

        public static CoreManager Core { get; private set; }

        public static Stats.Stats Stats { get; set; }
        internal static MainView MainView { get; set; }
        public static ProfileManagerView ProfileManagerView { get; set; }
        public static StatsView StatsView { get; set; }
        public static DoThingsBot DoThingsBot { get; set; }
    }
}
